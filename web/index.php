<?php
/**
 *
 * @author Pierre-Gildas MILLON <pgmillon@gmail.com>
 */

$loader = require_once __DIR__.'/../vendor/autoload.php';

use Doctrine\Common\Annotations\AnnotationRegistry;
use JardinsAvalon\DashboardApplication;

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));
$app = new DashboardApplication(__DIR__.'/..');

$app->run();