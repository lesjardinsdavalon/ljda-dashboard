%define __os_install_post exit 0

Name: ljda-dashboard
Version: %{ver}
Release: 1
Source: ljda-dashboard-%{version}.zip
Summary: LJDA Dashboard
URL: http://lesjardinsdavalon.fr/
License: GPLv3

%description
Dashboard des Jardins d'Avalon

%prep
%setup

%install
%{__install} -d %{buildroot}/usr/local/share/ljda-dashboard
%__cp -r * %{buildroot}/usr/local/share/ljda-dashboard

%clean
%{__rm} -rf "%{buildroot}" "%{_builddir}"

%files
%attr(-,nginx,nginx) /usr/local/share/ljda-dashboard