#!/bin/bash

rm -rf {BUILD,BUILDROOT,RPMS,SRPMS,tmp} || true
mkdir -p {BUILD,BUILDROOT,RPMS,SRPMS,tmp}

VERSION=$(cat package.json | python2 -m json_read "this['version']")

rpmbuild -ba --define="_topdir $(pwd)" --define="_sourcedir %{_topdir}/dist" --define="_specdir %{_topdir}/specs" --define="ver $VERSION" specs/ljda-dashboard.spec
