
var LIVERELOAD_PORT = 35729;
var mountFolder = function (connect, dir) {
  return connect.static(require('path').resolve(dir));
};


module.exports = function (grunt) {
  require('time-grunt')(grunt);

  // load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.util.linefeed = '\n';

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    bowerrc: grunt.file.readJSON('.bowerrc'),

    dirs: {
      'vendor': '<%= bowerrc.directory %>'
    },

    bower: {
      install: {
        options: {
          targetDir: '<%= dirs.vendor %>',
          install: true,
          verbose: false,
          cleanTargetDir: false,
          cleanBowerDir: false
        }
      }
    },

    compass: {
      design: {
        options: {
          sassDir: 'web/css',
          cssDir: 'web/css'
        }
      }
    },

    clean: {
      dist: ['dist']
    },

    copy: {
      dist: {
        files: [
          {
            expand: true,
            src: [
              'doc/**/*',
              'src/**/*',
              'vendor/**/*',
              'views/**/*',
              'web/index.php',
              'web/css/*.css',
              'web/components/**/*'
            ],
            filter: 'isFile',
            dest: 'dist/<%= pkg.name %>-<%= pkg.version %>'
          }
        ]
      }
    },

    compress: {
      dist: {
        options: {
          archive: 'dist/<%= pkg.name %>-<%= pkg.version %>.zip'
        },
        expand: true,
        cwd: 'dist/',
        src: ['**/*']
      }
    },

    connect: {
      livereload: {
        options: {
          port: 8002,
          hostname: '*',
          middleware: function (connect, options) {
            var serveStatic = require('serve-static');

            return [
              require('connect-livereload')({ port: LIVERELOAD_PORT }),
              require('grunt-connect-proxy/lib/utils').proxyRequest
            ];
          }
        },
        proxies: [
          {
            context: '/',
            host: '127.0.0.1',
            port: 8001
          }
        ]
      }
    },

    watch: {
      grunt: { files: ['Gruntfile.js'] },

      compass: {
        files: 'web/css/main.scss',
        tasks: ['compass']
      },

      styles: {
        files: [
          'web/css/main.css',
          'views/**/*',
          'config/**/*',
          'src/**/*',
          'web/index.php'
        ],
        options: {
          livereload: LIVERELOAD_PORT,
          nospawn: true
        }
      }
    }
  });

  grunt.registerTask('build', [
    'compass'
  ]);

  grunt.registerTask('serve', [
    'build',
    'configureProxies:livereload',
    'connect:livereload',
    'watch'
  ]);

  grunt.registerTask('package', [
    'clean:dist',
    'copy:dist',
    'compress:dist',
    'build'
  ]);

  grunt.registerTask('default', [
    'build'
  ]);

};
