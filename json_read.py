#!/usr/bin/env python2

import sys
import json

def main():
    if len(sys.argv) == 2:
        infile = sys.stdin
        outfile = sys.stdout
        path = sys.argv[1]
    elif len(sys.argv) == 3:
        infile = open(sys.argv[1], 'rb')
        outfile = sys.stdout
        path = sys.argv[2]
    else:
        raise SystemExit(sys.argv[0] + " [infile] 'property'")
    with infile:
        try:
            obj = json.load(infile)
        except ValueError, e:
            raise SystemExit(e)
    with outfile:
        # json.dump(obj, outfile, sort_keys=True,
        #           indent=4, separators=(',', ': '))
        # outfile.write('\n')
        outfile.write(eval(path, {},{"this": obj}) + '\n')


if __name__ == '__main__':
    main()