<?php
/**
 *
 * @author Pierre-Gildas MILLON <pgmillon@gmail.com>
 */

namespace JardinsAvalon\Controller;

use Doctrine\Common\Collections\Criteria;
use JardinsAvalon\Database\Entities\Newsletter\Newsletter;
use JardinsAvalon\Database\Entities\Newsletter\NewsletterSubscription;
use JardinsAvalon\Views\NewsletterSubscriptionsDTO;
use Silex\Application;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class NewsletterController
 * @package JardinsAvalon\Controller
 */
class NewsletterController extends AbstractController {

  const ENTITY_NAME='JardinsAvalon\Database\Entities\Newsletter\Newsletter';

  /**
   * @return \Doctrine\ORM\EntityRepository
   */
  public function getRepository() {
    return parent::getRepository(self::ENTITY_NAME);
  }

  public function getSubscriptionsDTO($subscriptions) {
    array_walk($subscriptions, function(&$subscription) {
      /* @var $subscription NewsletterSubscription */
      $subscription = new NewsletterSubscriptionsDTO(
        $subscription->getId(),
        $subscription->getNewsletter()->getId(),
        $subscription->getSentDate(),
        $subscription->getSubscriber()->getEmail()
      );
    });
    return $subscriptions;
  }

  /**
   * @Route("/", name="newsletter_list")
   */
  public function listNewsletters(Request $request) {
    $newsletters = $this->getRepository()->findBy(array(), array('id' => 'desc'));

    return $this->getResponse('newsletter/list', array(
      'newsletters' => $newsletters
    ));
  }

  /**
   * @Route("/{id}/pause", name="newsletter_pause")
   */
  public function pauseNewsletter($id) {
    $newsletter = $this->getRepository()->findOneById($id);
    /* @var $newsletter \JardinsAvalon\Database\Entities\Newsletter\Newsletter */

    $newsletter->setStatus(Newsletter::STATUS_PAUSE);
    $this->getEntityManager()->persist($newsletter);
    $this->getEntityManager()->flush();

    return new RedirectResponse($this->generateUrl('newsletter_list'));
  }

  /**
   * @Route("/{id}/subscriptions", name="newsletter_users")
   */
  public function showSubscriptions($id) {
    $newsletter = $this->getRepository()->findOneById($id);
    /* @var $newsletter \JardinsAvalon\Database\Entities\Newsletter\Newsletter */

    return $this->getResponse('newsletter/users', array(
      'newsletter_id' => $id,
      'subscriptions' => $this->getSubscriptionsDTO($newsletter->getSubscriptions()->toArray())
    ));
  }

  /**
   * @Route("/{id}/subscriptions/search", name="newsletter_subscriber_search", methods="POST")
   */
  public function searchSubscriptions($id, Request $request) {
    $form = $this->getFormBuilder()
      ->add('email')
      ->getForm();

    $data = $form->handleRequest($request)->getData();

    $qb = $this->getQueryBuilder();
    $qb->select(array('newsletter', 'subscription', 'subscriber'))
      ->from('JardinsAvalon\Database\Entities\Newsletter\NewsletterSubscription', 'subscription')
      ->leftJoin('subscription.subscriber', 'subscriber')
      ->leftJoin('subscription.newsletter', 'newsletter')
      ->where('newsletter.id = :newsletter_id')
      ->setParameter('newsletter_id', $id);

    $qb->andWhere($this->parseFilter('subscriber.email', $data['email']));

    return $this->getResponse('newsletter/users', array(
      'newsletter_id' => $id,
      'subscriptions' => $this->getSubscriptionsDTO($qb->getQuery()->getResult())
    ));
  }

  /**
   * @Route("/{id}/subscriptions/update", name="newsletter_subscriber_update", methods="POST")
   */
  public function updateSubscriptions($id, Request $request) {
    $newsletter = $this->getRepository()->findOneById($id);
    /* @var $newsletter \JardinsAvalon\Database\Entities\Newsletter\Newsletter */

    $form = $this->getFormBuilder()
      ->add('id', 'choice', array(
        'choices' => array_map(function($subscription) {
          return (string)$subscription->getId();
        }, $newsletter->getSubscriptions()->toArray()),
        'multiple' => true,
      ))
      ->add('id')
      ->add('resend', 'submit')
      ->getForm();

    $form->handleRequest($request);

    $subscriptions = $newsletter->getSubscriptions()->filter(function($subscription) use($form) {
      return in_array($subscription->getId(), $form['id']->getData());
    });

    switch(true) {
      case $form['resend']->isClicked():
        foreach($subscriptions as $subscription) {
          /* @var $subscription NewsletterSubscription */
          $subscription->setSentDate(null);
          $this->getEntityManager()->persist($newsletter);
          $this->getEntityManager()->flush();
        }
        break;
    }

    return new RedirectResponse($this->generateUrl('newsletter_users', array('id' => $id)));
  }

}