<?php
/**
 *
 * @author Pierre-Gildas MILLON <pgmillon@gmail.com>
 */

namespace JardinsAvalon\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use JardinsAvalon\DashboardApplication;
use Silex\Application;
use Symfony\Component\Form\FormBuilderInterface;

abstract class AbstractController {

  /**
   * @var Application
   */
  private $app;

  /**
   * @param DashboardApplication $app
   */
  function __construct($app) {
    $this->app = $app;
  }

  public function getResponse($template, $variables = array()) {
    $request = $this->app['request'];
    $extension = '.html';

    if(array_intersect(array('application/json', 'text/javascript'), $request->getAcceptableContentTypes())) {
      $paths = $this->app['app.root'].'/views/json';
      $request->setRequestFormat('json');
      $extension = '.json';
    }

    return $this->app['twig']->render($template.$extension.'.twig', $variables);
  }

  public function parseFilter($field, $input) {
    $queryBuilder = $this->getQueryBuilder();

    if(false !== strpos($input, '*')) {
      return $queryBuilder->expr()->like($field, $queryBuilder->expr()->literal(str_replace('*', '%', $input)));
    } elseif (0 < preg_match('/,/', $input)) {
      $emails = preg_split('/,/x', $input);
      array_walk($emails, function(&$value) {
        $value = trim($value);
      });
      return $queryBuilder->expr()->in($field, $emails);
    }

    return $queryBuilder->expr()->eq($field, $queryBuilder->expr()->literal($input));
  }

  public function generateUrl($routeName, $options = array()) {
    return $this->app['url_generator']->generate($routeName, $options);
  }

  /**
   * @param array $defaults
   * @return FormBuilderInterface
   */
  public function getFormBuilder($defaults = array()) {
    return $this->app['form.factory']->createBuilder('form', $defaults);
  }

  /**
   * @param $entityName
   * @return EntityRepository
   */
  public function getRepository($entityName) {
    return $this->getEntityManager()->getRepository($entityName);
  }

  /**
   * @return QueryBuilder
   */
  public function getQueryBuilder() {
    return $this->getEntityManager()->createQueryBuilder();
  }

  /**
   * @return EntityManager
   */
  public function getEntityManager()
  {
    return $this->app['orm.em'];
  }

}