<?php
/**
 *
 * @author Pierre-Gildas MILLON <pgmillon@gmail.com>
 */

namespace JardinsAvalon\Controller;

use Silex\Application;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController {

  /**
   * @Route("/", name="homepage")
   * @return mixed
   */
  public function listAction() {
    return $this->getResponse('index');
  }

}