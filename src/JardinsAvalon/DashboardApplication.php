<?php
/**
 *
 * @author Pierre-Gildas MILLON <pgmillon@gmail.com>
 */

namespace JardinsAvalon;

use Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Events;
use JardinsAvalon\Controller\HomeController;
use JardinsAvalon\Controller\NewsletterController;
use JardinsAvalon\Database\DoctrineExtensions\TablePrefix;
use JardinsAvalon\Database\Entities\Newsletter\Newsletter;
use JardinsAvalon\Routing\Loader\AnnotationClassLoader;
use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Yaml\Yaml;

class DashboardApplication extends Application {

  public function registerAnnotatedControllerService($serviceId, $service, $prefix = '/') {
    $this[$serviceId] = $service;

    $this['routes'] = $this->extend('routes', function (RouteCollection $routes) use ($prefix, $serviceId, $service) {
      $loader = new AnnotationClassLoader(new AnnotationReader());
      $loader->setServiceId($serviceId);

      $collection = $loader->load(get_class($service), $serviceId);
      $collection->addPrefix($prefix);

      $routes->addCollection($collection);

      return $routes;
    });

  }

  public function getConfig($file) {
    return Yaml::parse($this->getAppRoot() . '/config/' . $file);
  }

  public function __construct($appRoot = __DIR__, array $values = array()) {
    parent::__construct($values);
    $self = $this;

    $this['app.root'] = realpath($appRoot);

    $this->register(new UrlGeneratorServiceProvider());

    $this->register(new ServiceControllerServiceProvider());

    $this->register(new FormServiceProvider());

    $this->register(new TwigServiceProvider(), array(
      'twig.path' => $this['app.root'] . '/views',
    ));

    $this['twig']->addExtension(new \Twig_Extension_Debug());

    $this->register(new DoctrineServiceProvider(), array(
      "db.options" => $this->getConfig('database.yml'),
    ));

    $this->register(new DoctrineOrmServiceProvider(), array(
      "orm.em.options" => array(
        "mappings" => array(
          array(
            "type" => "annotation",
            "namespace" => "JardinsAvalon\\Database",
            "path" => $appRoot . "/src/JardinsAvalon/Database",
            'use_simple_annotation_reader' => false
          ),
        ),
      ),
    ));

    $this['form.extensions'] = $this->share(function ($app) {
      return array(
        new HttpFoundationExtension(),
      );
    });

    $this['dbs.event_manager']['default']->addEventListener(Events::loadClassMetadata, new TablePrefix('magento_'));

    $this['twig']->addFilter(new \Twig_SimpleFilter('newsletter_status', function ($string) {
      switch ($string) {
        case Newsletter::STATUS_NEVER:
          return 'Not Sent';
        case Newsletter::STATUS_SENDING:
          return 'Sending';
        case Newsletter::STATUS_CANCEL:
          return 'Cancelled';
        case Newsletter::STATUS_SENT:
          return 'Sent';
        case Newsletter::STATUS_PAUSE:
          return 'Paused';
      }
    }));

    $this->registerAnnotatedControllerService('home.controller', new HomeController($this));
    $this->registerAnnotatedControllerService('newsletter.controller', new NewsletterController($this), '/newsletter');

    $this['debug'] = true;

  }

  /**
   * @return string
   */
  public function getAppRoot() {
    return $this['app.root'];
  }

}