<?php
/**
 *
 * @author Pierre-Gildas MILLON <pgmillon@gmail.com>
 */

namespace JardinsAvalon\Database\Entities\Newsletter;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Subscriber
 * @package JardinsAvalon\Database\Entities\Newsletter
 * @ORM\Entity
 * @ORM\Table(name="newsletter_subscriber")
 */
class Subscriber {

  /**
   * @ORM\Id
   * @ORM\Column(type="integer", name="subscriber_id")
   */
  private $id;

  /**
   * @ORM\OneToMany(targetEntity="NewsletterSubscription", mappedBy="subscriber")
   **/
  private $subscriptions;

  /**
   * @ORM\Column(type="string", name="subscriber_email")
   **/
  private $email;

  /**
   * @return mixed
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return mixed
   */
  public function getEmail() {
    return $this->email;
  }

  /**
   * @return mixed
   */
  public function getSubscriptions() {
    return $this->subscriptions;
  }

}