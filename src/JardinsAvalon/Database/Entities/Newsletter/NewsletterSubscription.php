<?php
/**
 *
 * @author Pierre-Gildas MILLON <pgmillon@gmail.com>
 */

namespace JardinsAvalon\Database\Entities\Newsletter;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class NewsletterSubscriber
 * @package JardinsAvalon\Database\Entities\Newsletter
 * @ORM\Entity
 * @ORM\Table(name="newsletter_queue_link")
 */
class NewsletterSubscription {

  /**
   * @ORM\Id
   * @ORM\Column(type="integer", name="queue_link_id")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="Newsletter")
   * @ORM\JoinColumn(name="queue_id", referencedColumnName="queue_id")
   */
  private $newsletter;

  /**
   * @ORM\Column(type="datetime", name="letter_sent_at", nullable=true)
   */
  private $sentDate;

  /**
   * @ORM\ManyToOne(targetEntity="Subscriber", fetch="EAGER")
   * @ORM\JoinColumn(name="subscriber_id", referencedColumnName="subscriber_id")
   */
  private $subscriber;

  /**
   * @return mixed
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return Newsletter
   */
  public function getNewsletter() {
    return $this->newsletter;
  }

  /**
   * @return mixed
   */
  public function getSentDate() {
    return $this->sentDate;
  }

  /**
   * @param mixed $sentDate
   */
  public function setSentDate($sentDate) {
    $this->sentDate = $sentDate;
  }

  /**
   * @return Subscriber
   */
  public function getSubscriber() {
    return $this->subscriber;
  }

}