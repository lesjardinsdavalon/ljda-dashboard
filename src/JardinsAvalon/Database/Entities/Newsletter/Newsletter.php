<?php
/**
 *
 * @author Pierre-Gildas MILLON <pgmillon@gmail.com>
 */

namespace JardinsAvalon\Database\Entities\Newsletter;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Newsletter
 * @package JardinsAvalon\Database
 * @ORM\Entity
 * @ORM\Table(name="newsletter_queue")
 */
class Newsletter {

  /**
   * @ORM\Id
   * @ORM\Column(type="integer", name="queue_id")
   */
  private $id;

  /**
   * @ORM\Column(type="string", name="newsletter_subject")
   */
  private $subject;

  /**
   * @ORM\Column(type="string", name="queue_status")
   */
  private $status;

  /**
   * @ORM\OneToMany(targetEntity="NewsletterSubscription", mappedBy="newsletter", fetch="EAGER")
   **/
  private $subscriptions;

  const STATUS_NEVER = 0;
  const STATUS_SENDING = 1;
  const STATUS_CANCEL = 2;
  const STATUS_SENT = 3;
  const STATUS_PAUSE = 4;

  /**
   * @return mixed
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return mixed
   */
  public function getSubject() {
    return $this->subject;
  }

  /**
   * @return mixed
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * @param mixed $status
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  /**
   * @return NewsletterSubscription[]
   */
  public function getSubscriptions() {
    return $this->subscriptions;
  }

}