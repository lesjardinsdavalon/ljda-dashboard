<?php
/**
 *
 * @author Pierre-Gildas MILLON <pgmillon@gmail.com>
 */

namespace JardinsAvalon\Routing\Loader;


use Symfony\Component\Routing\Loader\AnnotationClassLoader as BaseAnnotationClassLoader;
use Symfony\Component\Routing\Route;

class AnnotationClassLoader extends BaseAnnotationClassLoader {

  private $serviceId;

  protected function configureRoute(Route $route, \ReflectionClass $class, \ReflectionMethod $method, $annot) {
    $controller = $class->getName() . '::' . $method->getName();

    if(false === is_null($this->getServiceId())) {
      $controller = $this->getServiceId().':'.$method->getName();
    }

    $route->addDefaults(array(
      '_controller' => $controller,
    ));
  }

  /**
   * @return mixed
   */
  public function getServiceId() {
    return $this->serviceId;
  }

  /**
   * @param mixed $serviceId
   */
  public function setServiceId($serviceId) {
    $this->serviceId = $serviceId;
  }

}