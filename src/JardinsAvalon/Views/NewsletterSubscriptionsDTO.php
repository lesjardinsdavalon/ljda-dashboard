<?php
/**
 *
 * @author Pierre-Gildas MILLON <pgmillon@gmail.com>
 */

namespace JardinsAvalon\Views;


class NewsletterSubscriptionsDTO {

  private $id;

  private $newsletterId;

  private $sentDate;

  private $subscriberEmail;

  /**
   * NewsletterSubscriptionsDTO constructor.
   * @param $newsletterId
   * @param $sentDate
   * @param $subscriberEmail
   */
  public function __construct($id, $newsletterId, $sentDate, $subscriberEmail) {
    $this->id = $id;
    $this->newsletterId = $newsletterId;
    $this->sentDate = $sentDate;
    $this->subscriberEmail = $subscriberEmail;
  }

  /**
   * @return mixed
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @return mixed
   */
  public function getNewsletterId() {
    return $this->newsletterId;
  }

  /**
   * @return mixed
   */
  public function getSentDate() {
    return $this->sentDate;
  }

  /**
   * @return mixed
   */
  public function getSubscriberEmail() {
    return $this->subscriberEmail;
  }

}